<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'admin'], function() {
    Voyager::routes();
});

Route::get('/', 'MainController@index');
Route::get('contacts', 'ContactController@index');
Route::get('certificates', 'CertificateController@index');
Route::get('galleries', 'GalleryController@index');
Route::get('catalog', 'ProductController@index');
Route::get('catalog/{product}', 'ProductController@show')->name('product');
Route::get('pages/{url}', 'PageController@index');
Route::post('request', 'RequestController@send');
Route::get('search', 'SearchController@index')->name('search');