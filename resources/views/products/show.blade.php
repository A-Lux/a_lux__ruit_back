@extends('layouts.app')

@section('content')
<main class="main">
    <div class="product">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-6">
                <div class="row">
                    <div class="col-sm-6 p-0">
                        <div class="product-img">
                            <img src="{{ asset('storage/'.$product->image) }}" alt="{{ $product->name }}">
                        </div>
                    </div>
                    <div class="col-sm-6 prod-info">
                        <!-- <span>Артикул: {{ $product->model_code }}</span> -->
                        <p>{{ $product->name }}</p>
                        <h2></h2>
                        <div class="btn-container">
                            @isset(json_decode($product->pdf_file)[0])
                            <a href="{{ asset('storage/'.json_decode($product->pdf_file)[0]->download_link) }}" download="{{ json_decode($product->pdf_file)[0]->original_name }}">Скачать каталог</a>
                            @endisset
                            <a href="#" class="btn-catalog"  data-toggle="modal" data-target="#exampleModal" data-whatever="@getbootstrap">Обратный звонок <img src="{{ asset('images/arrow-right-blue.png') }}" alt=""></a>
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Обратный звонок</h5>
                                    </div>
                                    <form class="cform">
                                        <div class="modal-body">
                                            <div class="form-group">
                                                <label for="recipient-name" class="col-form-label">Имя:</label>
                                                <input type="text" name="name" class="form-control" id="recipient-name" required>
                                                <label for="recipient-name" class="col-form-label">Телефон:</label>
                                                <input type="text" name="number_email" class="form-control" id="recipient-name" required>
                                            </div>
                                            <div class="form-group">
                                                <label for="message-text" class="col-form-label">Сообщение:</label>
                                                <textarea name="message" class="form-control" id="message-text" required></textarea>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                                            <button type="submit" class="btn btn-primarySend">Отправить</button>
                                        </div>
                                    </form>
                                    </div>
                                </div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 mt-4 p-0">
                    {{-- <p>
                        {!! $product->description !!}
                    </p> --}}
                    {{-- <div class="product-status">
                        <p>Наличие</p>
                        <p>{{ $product->is_in_stock ? 'Есть на складе' : 'Нет в наличии'}}</p>
                    </div> --}}
                    <!-- <div class="product-status">
                        <p>Производитель</p>
                        <p>{{ $product->manufacturer }}</p>
                    </div>
                    <div class="product-status">
                        <p>Артикуль</p>
                        <p>{{ $product->model_code }}</p>
                    </div>
                    <div class="product-status">
                        <p>Код товара</p>
                        <p>{{ $product->product_code }}</p>
                    </div>
                    <div class="product-status">
                        <p>Тип изделия</p>
                        <p>{{ $product->product_type }}</p>
                    </div> -->
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-6">
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home"
                            role="tab" aria-controls="nav-home" aria-selected="true">Технические характеристики</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile"
                            role="tab" aria-controls="nav-profile" aria-selected="false">Габаритные
                            и установочные размеры</a>
                        <!-- <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact"
                            role="tab" aria-controls="nav-contact" aria-selected="false">Промо материалы</a> -->
                    </div>
                </nav>
                <div class="tab-content" id="nav-tabContent">
                    <div class="tab-pane fade show active text-info" id="nav-home" role="tabpanel"
                        aria-labelledby="nav-home-tab">
                        {!! $product->specifications !!}
                    </div>
                    <div class="tab-pane fade text-info" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        {!! $product->installation_dimensions !!}
                    </div>
                    <!-- <div class="tab-pane fade text-info" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                        {!! $product->promotional_materials !!}
                    </div> -->
                </div>
            </div>
        </div>
    </div>
    @include('partials.socials')
</main>
@endsection

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js" integrity="sha256-S1J4GVHHDMiirir9qsXWc8ZWw74PHHafpsHp5PXtjTs=" crossorigin="anonymous"></script>
<script>
$(".cform").on('submit', function(e) {
    e.preventDefault();
    var data = new FormData(e.target);

    axios.post('/request', data)
        .then(function(r) {
            $('#exampleModal').modal('hide');
            $('.modal-backdrop')[0].classList.remove('show');
            Swal.fire(
                'Успешно!',
                r.data.message,
                'success'
            )
        });
});
</script>
@endpush