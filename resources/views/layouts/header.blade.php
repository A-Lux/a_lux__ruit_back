<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('css/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/hc-offcanvas-nav.css') }}">
    <title>@yield('title', 'Ruit')</title>
</head>
<nav id="main-nav" defer>
    <ul>
        @foreach (menu('site', '_json') as $item)
            <li><a href="{{ url($item->url) }}">{{ $item->title }}</a></li>
        @endforeach
    </ul>
</nav>

<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-sm-2">
                <div class="header__logo">
                    <a href="{{ url('/') }}">
                        <img src="{{ asset('images/logo.svg') }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-sm-7">
                <form action="{{ route('search') }}" method="GET">
                    <input type="text" class="search_field" name="q" placeholder="Поиск..." value="{{ request()->q }}">
                </form>
            </div>
            <div class="col-sm-3 px-0">
            <h2>
           
            </h2>
            </div>
        </div>
    </div>
</header>