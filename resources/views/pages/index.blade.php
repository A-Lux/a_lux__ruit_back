@extends('layouts.app')

@section('content')
<main class="main about">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-8 px-0">
                <h1>{{ $page->title }}</h1>
                <div class="about_text">
                    {!! $page->content !!}
                </div>
            </div>
        </div>
    </div>
    @include('partials.socials')
</main>
@endsection
@push('scripts')
<script>
    new Glide('.catalog', {
        type: "carousel",
        perView: 4
    }).mount();
</script>
@endpush