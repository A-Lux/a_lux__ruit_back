@extends('layouts.app')

@section('content')
<main class="main">
    <div class="container">
        <div class="row">
        
        </div>
    </div>
    <div class="container">
        <h1>Сертификаты</h1>
        <div class="row">
            <div class="cascade-slider_container" id="cascade-slider">
                <div class="cascade-slider_slides">
                    @foreach ($certificates as $certificate)
                        <div class="cascade-slider_item">
                            @if(json_decode($certificate->pdf_file)[0])
                            @php 
                                $pdf = json_decode($certificate->pdf_file)[0]
                            @endphp
                            <a href="{{ asset('storage/'.$pdf->download_link) }}" download="{{ $pdf->original_name }}"><img src="{{ asset('storage/'.$certificate->image) }}"></a>
                            @else
                            <img src="{{ asset('storage/'.$certificate->image) }}">
                            @endif
                        </div>
                    @endforeach
                </div>
                <span class="cascade-slider_arrow cascade-slider_arrow-left" data-action="prev"></span>
                <span class="cascade-slider_arrow cascade-slider_arrow-right" data-action="next"></span>
            </div>
        </div>
    </div>
    @include('partials.socials')
</main>
@endsection
@push('scripts')
<script src="{{ asset('js/cascade-slider.js') }}"></script>
<script>
$('#cascade-slider').cascadeSlider({
    
});
</script>
@endpush