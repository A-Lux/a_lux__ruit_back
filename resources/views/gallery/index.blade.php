@extends('layouts.app')

@section('content')
<main class="main">
    <div class="container">
        <h1>Галерея</h1>
        <div class="row">
            <div class="col-sm-10">
                <div class="slider-for">
                    @foreach ($gallery as $item)
                    <div><img src="{{ asset('storage/'.$item->image) }}"></div>
                    @endforeach
                </div>
            </div>
            <div class="col-sm-2">
                <div class="slider-nav">
                    @foreach ($gallery as $item)
                    <div><img src="{{ asset('storage/'.$item->image) }}"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @include('partials.socials')
</main>
@push('scripts')
<script src="/js/slick.min.js"></script>
<script>
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav',
    });
    $('.slider-nav').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        centerMode: true,
        focusOnSelect: true,
        draggable: false
    });
</script>
@endpush
@endsection