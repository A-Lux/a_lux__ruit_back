@extends('layouts.app')

@section('content')
<main class="main">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="glide1">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            @isset($sliders[0])
                            @foreach ($sliders[0] as $item)
                            <li class="glide__slide main-slide"><img src="{{ asset('storage/'.$item->image) }}"></li>
                            @endforeach
                            @endisset
                        </ul>
                    </div>

                    <div class="glide__arrows" data-glide-el="controls">
                        <button class="glide__arrow glide__arrow--left" data-glide-dir="<"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                        <button class="glide__arrow glide__arrow--right" data-glide-dir=">"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="glide2">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            @isset($sliders[1])
                            @foreach ($sliders[1] as $item)
                            <li class="glide__slide main-slide"><img src="{{ asset('storage/'.$item->image) }}"></li>
                            @endforeach
                            @endisset
                        </ul>
                    </div>

                    <div class="glide__arrows" data-glide-el="controls">
                        <button class="glide__arrow glide__arrow--left" data-glide-dir="<"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                        <button class="glide__arrow glide__arrow--right" data-glide-dir=">"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                    </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="glide3">
                    <div class="glide__track" data-glide-el="track">
                        <ul class="glide__slides">
                            @isset($sliders[2])
                            @foreach ($sliders[2] as $item)
                            <li class="glide__slide main-slide"><img src="{{ asset('storage/'.$item->image) }}"></li>
                            @endforeach
                            @endisset
                        </ul>
                    </div>

                    <div class="glide__arrows" data-glide-el="controls">
                        <button class="glide__arrow glide__arrow--left" data-glide-dir="<"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                        <button class="glide__arrow glide__arrow--right" data-glide-dir=">"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                    </div>
                </div>
            </div>
        </div>
        <div class="absolute__banner">
                <p>
                    Алюминиевая
                </p>
                <p>
                    продукция «Ruit»
                </p>
            </div>
    </div>

    @include('partials.socials')
</main>
@endsection
@push('scripts')
<script>
    new Glide('.glide1', {
        type: "carousel"
    }).mount();
    new Glide('.glide2', {
        type: "carousel"
    }).mount();
    new Glide('.glide3', {
        type: "carousel"
    }).mount();
</script>
@endpush