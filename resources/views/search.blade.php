@extends('layouts.app')

@section('content')
<main class="main catalog">
    <div class="col-sm-12 px-0">
        <div class="catalog">
            <h1>Поиск</h1>
            @if (isset($error))
            <p>
                <span class="text-danger">{{ $error }}</span>
            </p>
            @elseif($products->count() == 0)
            <p>
                <span class="text-danger">По запросу {{ request()->q }} ничего не найдено!</span>
            </p>
            @else
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    @foreach ($products as $item)
                    <div>
                        <div class="glide__slide glide-catalog">
                            <a href="{{ route('product', $item->id) }}"><img src="{{ asset('storage/'.$item->image) }}" alt="{{ $item->name }}"></a>
                        </div>
                        <div>
                            <a href="{{ route('product', $item->id) }}">{{ $item->name }}</a>
                        </div>
                    </div>
                    @endforeach
                </ul>
            </div>

            <div class="glide__arrows" data-glide-el="controls">
                <button class="glide__arrow glide__arrow--left" data-glide-dir="<"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
                <button class="glide__arrow glide__arrow--right" data-glide-dir=">"><img src="{{ asset('images/arrow_empty.png') }}" alt="" srcset=""></button>
            </div>
            @endif
        </div>
    </div>
    @include('partials.socials')
</main>
@endsection
@push('scripts')
<script>
    new Glide('.catalog', {
        gap: 39,
        type: "carousel",
        perView: 4,
        breakpoints: {
            1624: {
                perView: 4
            },
            900: {
                perView:3
            },
            600: {
                perView: 1
            }
        }
    }).mount();
</script>
@endpush