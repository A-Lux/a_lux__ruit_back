@extends('layouts.app')

@section('content')
<main class="contacts contacts-desktop">
    <h1>Контакты</h1>
    <style>
        iframe {
            width: 100%;
            height: 500px;
        }
    </style>
    <a class="dg-widget-link" href="http://2gis.kz/almaty/firm/9429940000794248/center/76.98148012161256,43.29143544796721/zoom/16?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=bigMap">Посмотреть на карте Алматы</a>
    <div class="dg-widget-link"><a href="http://2gis.kz/almaty/firm/9429940000794248/photos/9429940000794248/center/76.98148012161256,43.29143544796721/zoom/17?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=photos">Фотографии компании</a></div>
    <div class="dg-widget-link"><a href="http://2gis.kz/almaty/center/76.984097,43.292547/zoom/16/routeTab/rsType/bus/to/76.984097,43.292547╎Sieger WDF, торгово-производственная компания?utm_medium=widget-source&utm_campaign=firmsonmap&utm_source=route">Найти проезд до Sieger WDF, торгово-производственная компания</a></div>
    <script charset="utf-8" src="https://widgets.2gis.com/js/DGWidgetLoader.js"></script>
    <script charset="utf-8">
        new DGWidgetLoader({
            "width": "100%",
            "height": 600,
            "borderColor": "#a3a3a3",
            "pos": {
                "lat": 43.29143544796721,
                "lon": 76.98148012161256,
                "zoom": 16
            },
            "opt": {
                "city": "almaty"
            },
            "org": [{
                "id": "9429940000794248"
            }]
        });
    </script><noscript style="color:#c00;font-size:16px;font-weight:bold;">Виджет карты использует JavaScript. Включите его в настройках вашего браузера.</noscript>
    <div class="absolute-contact">
        <div class="arrow-exit"></div>
        <div class="row">
            <div class="col-sm-5">
                <div class="contact-info">
                    <p><span>{{ $contacts->country }}</span></p>
                    <p>{{ $contacts->address }}</p>

                    <p><a class="text-white" href="tel:{{ $contacts->fphone }}">{{ $contacts->fphone }}</a></p>
                    <p><a class="text-white" href="tel:{{ $contacts->sphone }}">{{ $contacts->sphone }}</a></p>
                </div>
            </div>
            <div class="col-sm-7">
                <form class="cform">
                    <div class="contact_form">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" name="name" placeholder="Ваше имя" id="name" required>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" name="number_email" placeholder="Контактный телефон" id="number_email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea name="message" id="message" cols="30" rows="10" placeholder="Сообщение" required></textarea>
                                <div class="form-btn">
                                    <button type="submit">Отправить </button>
                                    <img src="{{ asset('images/arrow-right.png') }}" alt="">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>


    @include('partials.socials')


</main>

<main class="contacts contacts-mobile">
    <div class="absolute-contact-mobile">
        <div class="row">
            <div class="col-sm-12">
                <div class="contact-info">
                    <p><span>{{ $contacts->country }}</span></p>
                    <p>{{ $contacts->address }}</p>

                    <p>{{ $contacts->phone1 }}</p>
                    <p>{{ $contacts->phone2 }}</p>
                </div>
            </div>
            <div class="col-sm-12">
                <form class="cform">
                    <div class="contact_form">
                        <div class="row">
                            <div class="col-sm-6">
                                <input type="text" name="name" placeholder="Ваше имя" id="name" required>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" name="number_email" placeholder="Контактный телефон или эл.почта" id="number_email" required>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <textarea name="message" id="message" cols="30" rows="10" placeholder="Сообщение" required></textarea>
                                <button type="submit">Отправить</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('partials.socials')
</main>
@endsection
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js" integrity="sha256-S1J4GVHHDMiirir9qsXWc8ZWw74PHHafpsHp5PXtjTs=" crossorigin="anonymous"></script>
<script>
    document.querySelector(".contact-info").addEventListener("click", function() {
        document.querySelector(".absolute-contact").classList.toggle('unfold');
    })


    $(".cform").on('submit', function(e) {
        e.preventDefault();
        var data = new FormData(e.target);

        axios.post('/request', data)
            .then(function(r) {
                Swal.fire(
                    'Успешно!',
                    r.data.message,
                    'success'
                )
            });
    });
</script>
@endpush