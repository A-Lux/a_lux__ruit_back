<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class SiteRequest extends Model
{
    protected $fillable = ['name', 'number_email', 'message', 'position'];  
}
