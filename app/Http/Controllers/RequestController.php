<?php

namespace App\Http\Controllers;

use App\SiteRequest;
use Illuminate\Http\Request;

class RequestController extends Controller
{
    public function send(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'number_email' => 'required',
            'message' => 'required'
        ]);
        
        SiteRequest::create($request->all());
        return response(['message' => 'Сообщение отправлено'], 200);
    }
}
