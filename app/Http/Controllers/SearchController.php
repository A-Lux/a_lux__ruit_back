<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index(Request $request) {
        if(strlen($request->q) == 0) {
            return view('search', ['error' => 'Пустой поисковый запрос (запрос не содержит букв или цифр).']);
        }
        $products = Product::where('name', 'LIKE', '%'.$request->q.'%')->get();
        return view('search', compact('products'));
    }
}
