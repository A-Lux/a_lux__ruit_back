<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index() {
        $sliders = Slider::get()->groupBy('type');
        return view('main.index', compact('sliders'));
    }
}
