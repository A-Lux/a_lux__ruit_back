<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index($url) {
        $page = Page::where('url', $url)->first();
        
        if($page) {
            return view('pages.index', compact('page'));
        }

        return abort(404);
    }
}
