<?php

namespace App\Http\Controllers;

use App\Certificate;
use Illuminate\Http\Request;

class CertificateController extends Controller
{
    public function index() {
        $certificates = Certificate::get();

        return view('certificates.index', compact('certificates'));
    }
}
